package com.example.real_kwizzard;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class  QuizActivity extends AppCompatActivity {
    private Button gameOverButton;

    private static final long COUNTDOWN_IN_MILLIS = 15000;

    private TextView tvQuestion, tvScore, tvQuestionNo, tvTimer;
    private RadioGroup radioGroup;
    private RadioButton rb1, rb2, rb3;
    private Button btnNext;

    private ColorStateList textColorDefaultCd;
    private ColorStateList textColorWhite;
    private CountDownTimer countDownTimer;
    private long timeLeftInMillis;

    int totalQuestion;
    int qCounter= 0;
    int score;


    ColorStateList dfrbColor;
    boolean answered;


    private QuestionModel currentQuestion;


    private List<QuestionModel> questionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        questionList = new ArrayList<>();
        tvQuestion = findViewById(R.id.textQuestion);
        tvScore = findViewById(R.id.textScore);
        tvQuestionNo = findViewById(R.id.textQestionNo);
        tvTimer = findViewById(R.id.textTimer);

        radioGroup = findViewById(R.id.radioGroup);
        rb1 = findViewById(R.id.rb1);
        rb2 = findViewById(R.id.rb2);
        rb3 = findViewById(R.id.rb3);
        btnNext = findViewById(R.id.btnNext);

        dfrbColor = rb1.getTextColors();
        textColorDefaultCd = tvTimer.getTextColors();


        addQuestion();
        totalQuestion = questionList.size();
        showNextQuestion();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(answered == false){
                    if(rb1.isChecked() || rb2.isChecked() || rb3.isChecked()){
                        checkAnswer();
                        countDownTimer.cancel();

                    } else {
                        Toast.makeText(QuizActivity.this, "Please select an option ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showNextQuestion();
                }
            }
        });
    }

    private void checkAnswer() {
        answered = true;

        countDownTimer.cancel();

        RadioButton rbSelected = findViewById(radioGroup.getCheckedRadioButtonId());
        int answerNo = radioGroup.indexOfChild(rbSelected) + 1;
        if(answerNo == currentQuestion.getCorrectAnsNo()){
            score++;
            tvScore.setText("Score: " + score);
        }
        rb1.setTextColor(Color.RED);
        rb2.setTextColor(Color.RED);
        rb3.setTextColor(Color.RED);
        switch (currentQuestion.getCorrectAnsNo()){
            case 1:
                rb1.setTextColor(Color.GREEN);
                break;
            case 2:
                rb2.setTextColor(Color.GREEN);
                break;
            case 3:
                rb3.setTextColor(Color.GREEN);
                break;
        }
        if(qCounter < totalQuestion){
            btnNext.setText("Next");
        } else {
            btnNext.setText("Finish");
        }
    }

    private void showNextQuestion() {

        radioGroup.clearCheck();
        rb1.setTextColor(Color.WHITE);
        rb2.setTextColor(Color.WHITE);
        rb3.setTextColor(Color.WHITE);

        if(qCounter < totalQuestion){

            currentQuestion = questionList.get(qCounter);
            tvQuestion.setText(currentQuestion.getQuestion());
            rb1.setText(currentQuestion.getOption1());
            rb2.setText(currentQuestion.getOption2());
            rb3.setText(currentQuestion.getOption3());

            qCounter++;
            btnNext.setText("Submit");
            tvQuestionNo.setText("Question: " + qCounter + "/" + totalQuestion);
            answered = false;

            timeLeftInMillis = COUNTDOWN_IN_MILLIS;
            startCountDown();

        } else {
            finish();
            Intent intent = new Intent(QuizActivity.this, GameOver.class);
            intent.putExtra("keyname", score);
            startActivity(intent);

        }
    }



    private void startCountDown(){
        countDownTimer = new CountDownTimer(timeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                timeLeftInMillis = 0;
                updateCountDownText();
                checkAnswer();
            }
        }.start();
    }

    private void updateCountDownText(){
        int minutes = (int)(timeLeftInMillis / 1000) / 60;
        int seconds = (int)(timeLeftInMillis / 1000) % 60;

        String timerFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        tvTimer.setText(timerFormatted);

        if (timeLeftInMillis < 9000){
            tvTimer.setTextColor(Color.RED);
        } else {
            tvTimer.setTextColor(Color.WHITE);
        }
    }


    private void addQuestion() {
        questionList.add(new QuestionModel("What is the only English word, with two synonyms that are antonyms of each other?", "A. Cleave", "B. Similar", "C. Logo", 1));
        questionList.add(new QuestionModel("If a rooster lays an egg on the exact peak of a barn, which side does it fall?", "A. Roosters don't lay eggs.", "B. Right side", "C. Left side", 1));
        questionList.add(new QuestionModel("I drive men mad for love of me, easily beaten, never free.", "A. Women", "B. Love", "C. Gold", 3));
        questionList.add(new QuestionModel("It is weightless, you can see it. If you put it in a barrel it will make it lighter?", "A. Hole", "B. Water", "C. Cotton", 1));
        questionList.add(new QuestionModel("Take one out and scratch my head I am now black but once was red.?", "A. Rubber", "B. Match", "C. Lighter", 2));
        questionList.add(new QuestionModel("Where can you find cities, towns, shops and streets but no people?", "A. Nowhere", "B. Lost City", "C. Map", 3));
        questionList.add(new QuestionModel("If two's company, and three's a crowd, what are four and five?", "A. Five", "B. Nine", "C. Seven", 2));
        questionList.add(new QuestionModel("What is the difference between a dollar and a half and thirty-five-cents?", "A. Half-dollar", "B. Nothing", "C. Thousands", 2));
        questionList.add(new QuestionModel("How many times can you subtract five from twenty-five?", "A. Twice", "B. Five", "C. Once", 3));
        questionList.add(new QuestionModel("When is 1500 plus 20 and 1600 minus 40 the same thing?", "A. American Time", "B. Military Time", "C. Filipino Time", 2));
        questionList.add(new QuestionModel("What 3 positive numbers give the same result when multiplied and added together? ", "A. 1, 7 and 4", "B. 1, 2 and 3", "C. 1, 8 and 2", 2));
        questionList.add(new QuestionModel("Divide 20 by half and add 30, what do you get", "A. 50", "B. 90", "C. 70", 3));
        questionList.add(new QuestionModel("If you divide thirty by half, and add ten, what do you get?", "A. 60", "B. 25", "C. 70", 3));
        questionList.add(new QuestionModel("If Mr. Flynn is 90 centimeters plus half his height, how tall is he? ", "A. 150 centimeters", "B. 180 centimeters", "C. 135 centimeters", 2));
        questionList.add(new QuestionModel("A word I know, six letters it contains. Subtract just one, and twelve is what remains.", "A. Twenty", "B. Thirty", "C. Dozens", 3));
        questionList.add(new QuestionModel("Three times what number is no larger than two times that same number? ", "A. 0", "B. 2", "C. 1", 1));
        questionList.add(new QuestionModel("What do mathematics teachers like to eat?", "A. Apples", "B. Pie", "C. Watermelon", 2));
        questionList.add(new QuestionModel("What is the value of 1/2 of 2/3 of 3/4 of 4/5 of 5/6 of 6/7 of 7/8 of 8/9 of 9/10 of 1000? ", "A. 99", "B. 100", "C. 69", 2));
        questionList.add(new QuestionModel("Use the numbers 2, 3, 4 and 5 and the symbols + and = to make a true equation. ", "A. 4 + 2 = 5 + 3", "B. 2 + 3 = 5 + 4", "C. 2 + 5 = 3 + 4", 3));
        questionList.add(new QuestionModel("What did one math book say to another? ", "A. I have so many problems.", "B. You look hard", "C. I can’t understand you", 1));
        questionList.add(new QuestionModel("What is half of two plus two? ", "A. One", "B. Two", "C. Three", 3));
        questionList.add(new QuestionModel("Four years ago, Alex was twice as old as Jake. Four years from now, Jake will be 3/4 of Alex's age. How old is Alex?", "A. 15", "B. 12", "C. 10", 2));
        questionList.add(new QuestionModel("How many 9's are there between 1 and 100? ", "A. 1", "B. 20", "C. 10", 2));
        questionList.add(new QuestionModel("Break it and it gets better, immediately set and harder to break again.", "A. Record", "B. Clay", "C. Clock", 1));
        questionList.add(new QuestionModel("What common English verb becomes its own past tense by rearranging its letters?", "A. Eat", "B. Run", "C. Sang", 1));
        questionList.add(new QuestionModel("Why did the chicken cross the road?", "A. To lay an egg", "B. To get to the other side", "C. To get her chicks", 2));
        questionList.add(new QuestionModel("It occurs once in a minute, twice in a moment, but never in an hour?", "A. The letter O", "B. The letter I", "C. The letter M", 3));
        questionList.add(new QuestionModel("What is binary of 69?", "A. 1000111", "B. 1000101 ", "C. 1000100", 2));
        questionList.add(new QuestionModel("If David’s age is 27 years old in 2011. What was his age in 2003?", "A. 19 years old", "B. 20 years old", "C. 21 years old", 1));
        questionList.add(new QuestionModel("What is 7% equal to?", "A. 0.007", "B. 0.7", "C. 0.07", 3));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null){
            countDownTimer.cancel();
        }
    }
}
