package com.example.real_kwizzard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GameOver extends AppCompatActivity {

    Button btnTryAgain;
    private TextView totalScoreLabel;
    int finalScore;
    int totalScore;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        totalScoreLabel = findViewById(R.id.totalScore);

        int score = getIntent().getIntExtra("keyname", 0);

        SharedPreferences settings = getSharedPreferences("quizApp", Context.MODE_PRIVATE);
        totalScore = settings.getInt("totalScore", 0);
        totalScore += score;

        totalScoreLabel.setText("Total Score: " + totalScore);






        btnTryAgain = findViewById(R.id.tryAgain);

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GameOver.this, QuizActivity.class);
                startActivity(intent);
            }
        });
    }
}
